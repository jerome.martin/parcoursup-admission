FROM python:3.10-alpine as builder


RUN apk add --update gcc libc-dev libffi-dev

RUN pip wheel cffi


FROM python:3.10-alpine

ARG VERSION=0.1.0

WORKDIR /app

# Get all the prereqs
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
  wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.30-r0/glibc-2.30-r0.apk  && \
  wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.30-r0/glibc-bin-2.30-r0.apk  && \
  apk add glibc-2.30-r0.apk  && \
  apk add glibc-bin-2.30-r0.apk && \
  rm -f glibc-2.30-r0.apk glibc-bin-2.30-r0.apk

# And of course we need Firefox if we actually want to *use* GeckoDriver
RUN apk add firefox-esr

# Then install GeckoDriver
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.31.0/geckodriver-v0.31.0-linux64.tar.gz  && \
  tar -zxf geckodriver-v0.31.0-linux64.tar.gz -C /usr/bin  && \
  rm -f geckodriver-v0.31.0-linux64.tar.gz && \
  geckodriver --version


COPY --from=builder /*.whl /tmp/
RUN pip install /tmp/*.whl

COPY parcoursupadmission.sh /etc/periodic/daily-5AM/parcoursupadmission.sh

RUN mkdir /app/config && \
    chmod +x /etc/periodic/daily-5AM/parcoursupadmission.sh && \
    echo "0       5       *       *       *       run-parts /etc/periodic/daily-5AM" >> /etc/crontabs/root

CMD crond -f -l 8

COPY dist/parcoursupadmission-$VERSION-py3-none-any.whl /tmp
RUN pip install /tmp/parcoursupadmission-$VERSION-py3-none-any.whl && rm -f /tmp/parcoursupadmission-$VERSION-py3-none-any.whl

