import ovh
import json
import logging

logger = logging.getLogger("sms")


def send_sms(res: dict, config: dict):
    """

    :param res:
    :param config:
    :return:
    """
    provider = config["provider"]
    if provider == "ovh":
        __send_ovh(res, config)
    else:
        logger.error("Provider '%s' inconnu", provider)


def __send_ovh(res: dict, config: dict):
    # create a client using configuration
    client = ovh.Client()

    services = client.get("/sms")
    print(services)

    url = f"/sms/{services[0]}/jobs"
    result_send = client.post(url,
                              charset='UTF-8',
                              coding='7bit',
                              message="Votre message",
                              noStopClause=False,
                              priority='high',
                              receivers=["+33666244423"],
                              senderForResponse=False,
                              validityPeriod=2880,
                              sender="delaur.info"
                              )
    print(json.dumps(result_send, indent=4))
    # pour l'affichage du résultat de la transaction
