"""
Ce module permet d'envoyer les résultats par email.
"""

import datetime
import logging
import smtplib
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

logger = logging.getLogger("email")


def __send_smtp(res: dict, config: dict):
    """
    Envoi les résultats par email.

    :param res: les résultats
    :param config: la configuration smtp
    """
    now = datetime.date.today()

    smtp_server = config["serveur"]
    port = config.get("port", 587)
    sender_email = config["emetteur"]
    smtp_auth = config.get("smtp-auth").split(":")
    dest = config["dest"]

    html = f"""\
    <html>
    <head>
    <meta charset="utf-8" />
    </head>
    <body>
    <div>
    <h1>Situation au {now.strftime("%d/%m/%Y")}</h1>
    <table>
    <tr><th>Formation</th><th>Situation</th><th>Position</th></tr>
    """

    for k, v in res.items():
        html += f"""\
        <tr>
            <td>{v['formation']}</td>
            <td>{v['situation']}</td>
            <td>{v.get("position", "")}</td>
        </tr>
        """

    html += """
    </table>
    </div>
    </body
    </html>
    """

    # Create a secure SSL context
    context = ssl.create_default_context()
    # Create a multipart message and set headers
    message = MIMEMultipart('alternative')
    message["From"] = sender_email
    message["To"] = ", ".join(dest)
    message["Subject"] = "Situation parcoursup"

    part = MIMEText(html, 'html')
    message.attach(part)
    # Try to log in to server and send email
    try:
        server = smtplib.SMTP(smtp_server, port)
        server.ehlo()  # Can be omitted
        server.starttls(context=context)  # Secure the connection
        server.ehlo()  # Can be omitted
        server.login(smtp_auth[0], smtp_auth[1])
        server.send_message(message)
        server.quit()
    except Exception as e:
        # Print any error messages to stdout
        logger.error("Exception raised: %s", e)


def send_email(res: dict, config: dict):
    """
    Envoi les résultats par email.

    :param res: les résultats
    :param config: la configuration smtp
    """
    provider = config.get("provider", "smtp")
    if provider == "smtp":
        __send_smtp(res, config)
    else:
        logger.error("Provider '%s' inconnu", provider)
