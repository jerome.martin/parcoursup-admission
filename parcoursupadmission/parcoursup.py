"""
Ce module fournit la fonction recuperation_parcoursup.
"""
import logging
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

logger = logging.getLogger("parcoursup")


def __highlight(element, effect_time, color, border):
    """Highlights (blinks) a Selenium Webdriver element"""
    driver = element._parent

    def apply_style(s):
        driver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)

    original_style = element.get_attribute('style')
    apply_style("border: {0}px solid {1};".format(border, color))
    time.sleep(effect_time)
    apply_style(original_style)


def recuperation_parcoursup(ndossier: str, mdp: str, executable_path: str = None, navigator: bool = False) -> dict:
    """
    Récupère les informations dans parcoursup.

    :param ndossier: le numéro du dossier
    :param mdp: le mot de passe**
    :param executable_path: Chemin du navigateur
    :param navigator: True pour afficher le navigateur (mode debug)
    :return: un dictionnaire contenant la situation associé au numéro de voeu
    """
    res = dict()
    options = Options()
    options.headless = not navigator
    browser = webdriver.Firefox(options=options, executable_path=executable_path)
    try:
        browser.get('https://dossierappel.parcoursup.fr')

        # Valider les cookies
        element = WebDriverWait(browser, 300).until(
            lambda x: x.find_element(by=By.ID, value='tarteaucitronPersonalize2'))
        __highlight(element, 1, "blue", 2)
        element.click()
        logger.debug("Cookies validés")

        # Saisir les identifiants
        element = WebDriverWait(browser, 300).until(lambda x: x.find_element(by=By.NAME, value='g_cn_cod'))
        element.send_keys(ndossier)
        element = browser.find_element(by=By.NAME, value='g_cn_mot_pas')
        element.send_keys(mdp)
        element = browser.find_element(by=By.ID, value='btnConnexion')
        element.click()
        logger.debug("Authentification ok")

        # Parcours de voeux
        element = browser.find_element(by=By.ID, value='btn_toggle_enattente')
        __highlight(element, 3, "blue", 2)
        element = WebDriverWait(browser, 300).until(lambda x: x.find_element(by=By.ID, value='btn_toggle_enattente'))
        element.click()
        logger.debug("Affichage des voeux ok")

        element = browser.find_element(by=By.ID, value='voeux_enattente')
        table = element.find_element(by=By.TAG_NAME, value="tbody")
        voeux = table.find_elements(by=By.TAG_NAME, value="tr")
        logger.debug("Parcours des %s voeux", len(voeux))
        for voeu in voeux:
            __highlight(voeu, 3, "blue", 2)
            voeu_id = voeu.get_attribute("id").replace("-", "_")

            formation = voeu.find_element(by=By.XPATH, value="./td[3]").text
            situation = voeu.find_element(by=By.XPATH, value="./td[4]").text

            res[voeu_id] = {
                "formation": formation,
                "situation": situation
            }
            if situation == "En liste d'attente":
                logger.debug("Affichage de %s", f"btn_lst_att_{voeu_id}")

                bouton = WebDriverWait(browser, 300).until(
                    lambda x: x.find_element(by=By.ID, value=f"btn_lst_att_{voeu_id}"))
                __highlight(bouton, 1, "blue", 2)
                bouton.click()

                position = voeu.find_element(by=By.XPATH, value="./td[5]/div/div/ul/li/div/ul/li[1]").text
                position_elm = position.split(":")
                position_idx = position_elm[1].strip()
                res[voeu_id]["position"] = position_idx
                logger.info(f"{voeu_id} -> {formation}: {situation} -> {position_idx}")

                try:
                    element = browser.find_element(by=By.ID, value="tarteaucitronCloseAlert")
                    element.click()

                    WebDriverWait(browser, 300).until_not(
                        expected_conditions.presence_of_element_located((By.ID, "tarteaucitronCloseAlert")))
                except:
                    pass

                bouton = voeu.find_element(by=By.XPATH, value="./td[5]/div/div/ul/li/div/div[2]/button[2]")
                bouton.click()
            else:
                logger.info(f"{voeu_id} -> {formation}: {situation}")
                logger.warning("Situation %s inconnue", situation)
    except Exception as e:
        logger.error("Exception raised: %s", e)
        import traceback
        traceback.print_exception(e)

    browser.close()
    return res
