"""
Programme principal.
"""

import datetime
import logging
import sqlite3
from pathlib import Path
import argparse
import toml
import os

from parcoursupadmission.send_email import send_email
from parcoursupadmission.parcoursup import recuperation_parcoursup
from parcoursupadmission.send_sms import send_sms

logger = logging.getLogger("main")


def main():
    if os.name == "nt":
        geckopath = r'C:\utils\geckodriver.exe'
    else:
        geckopath = "/usr/bin/geckodriver"

    parser = argparse.ArgumentParser()
    parser.add_argument("--verbose", "-v", dest="verbose", action="store_true")
    parser.add_argument("--navigator", dest="navigator", action="store_true")
    parser.add_argument("--config-dir", dest="config_dir", default="./")
    parser.add_argument("--data-dir", dest="data_dir", default="./data/")

    args = parser.parse_args()

    db_path = Path(args.data_dir) / "parcoursup.sqlite"

    config = toml.load(os.path.join(args.config_dir, "parcoursup.toml"))

    try:
        ndossier = config['parcoursup']["ndossier"]
        mdp = config['parcoursup']["mdp"]
    except KeyError:
        logger.error("Configuration non valide")
        return -1

    now = datetime.date.today()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.WARNING,
                        format="%(asctime)s - %(levelname)s - %(module)s:%(lineno)d - %(message)s")
    logging.getLogger("selenium").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    res = recuperation_parcoursup(ndossier, mdp, executable_path=geckopath, navigator=args.navigator)

    logger.debug("Resultats : %s", res)
    if not db_path.exists():
        db_path.parent.mkdir(exist_ok=True, parents=True)
        con = sqlite3.connect(db_path)
        cur = con.cursor()
        try:
            cur.execute(
                '''CREATE TABLE parcoursup (voeu varchar(12), date_recup varchar(12), formation text, situation text, position integer default null, PRIMARY KEY (voeu, date_recup))''')
        except sqlite3.OperationalError as e:
            cur.close()
            con.close()
            logger.error("Creation de la table: %s", e)
            db_path.unlink()
            return -1
    else:
        con = sqlite3.connect(db_path)
        cur = con.cursor()

    query_list = list()
    for k, v in res.items():
        el = {
            "voeu": k,
            "date_recup": now.isoformat(),
            "formation": v["formation"],
            "situation": v["situation"],
        }
        try:
            el["position"] = v["position"]
        except KeyError:
            el['position'] = "admis"
            pass
        query_list.append(el)
    logger.info("Enregistrement dans la base de données")
    try:
        logger.debug("Insertion de %s", query_list)
        cur.executemany("insert into parcoursup values (:voeu, :date_recup, :formation, :situation, :position)",
                        query_list)
        con.commit()
    except Exception as e:
        logger.error("Exception raised: %s", e)
        import traceback
        traceback.print_exception(e)

    cur.close()
    con.close()

    notifs = config.get("notification", list())
    for notif in notifs:
        logger.debug("Notification: %s", notif)
        notif_type = notif["type"]
        if notif_type == "sms":
            send_sms(res, notif["sms"])
        elif notif_type == "email":
            send_email(res, notif["email"])
        else:
            logger.error("Notification '%s' inconnue", notif_type)


if __name__ == '__main__':
    main()
